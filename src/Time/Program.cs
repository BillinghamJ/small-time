﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using Newtonsoft.Json;

namespace Time
{
	public class Program
	{
		static void Main()
		{
		    var sunTimes = GetJebSunTimes();

            Console.WriteLine("Rise: {0:d1},{1:d2}; Set: {2:d1},{3:d2}", sunTimes.Sunrise.Hours, sunTimes.Sunrise.Minutes, sunTimes.Sunset.Hours, sunTimes.Sunset.Minutes);
            Console.WriteLine();
		    Console.CursorVisible = false;

            while (true)
            {
                Console.WriteLine(new JebDateTime(DateTime.Now));
                Console.CursorTop--;
                Thread.Sleep(864);
            }
		}

        private static JebSunTimes GetJebSunTimes()
        {
            var sunTimes = GetSunTimes();
            var rise = new JebDateTime(sunTimes.Sunrise);
            var set = new JebDateTime(sunTimes.Sunset);
            return new JebSunTimes { Sunrise = rise, Sunset = set };
        }

        private static SunTimes GetSunTimes()
        {
            var coords = GetLocation();
            var url = string.Format("http://www.earthtools.org/sun/{0}/{1}/{2:d/M}/0/{3}", coords.Latitude, coords.Longitude, DateTime.Today, DateTime.Today.IsDaylightSavingTime() ? "1" : "0");
            var sunTimes = JsonConvert.DeserializeAnonymousType(Download(string.Format("http://makeitjson.com/?url={0}", HttpUtility.UrlEncode(url))), new { success = false, result = new { sun = new { morning = new { sunrise = string.Empty }, evening = new { sunset = string.Empty } } } });
            
            if (!sunTimes.success)
                return new SunTimes();

            return new SunTimes
            {
                Sunrise = DateTime.Today.Add(TimeSpan.Parse(sunTimes.result.sun.morning.sunrise)),
                Sunset = DateTime.Today.Add(TimeSpan.Parse(sunTimes.result.sun.evening.sunset))
            };
        }

        private static Coords GetLocation()
        {
            var coords = JsonConvert.DeserializeAnonymousType(Download("http://freegeoip.net/json/"), new { longitude = double.NaN, latitude = double.NaN });
            return new Coords { Longitude = coords.longitude, Latitude = coords.latitude };
        }

        private static string Download(string url)
        {
            var request = WebRequest.CreateHttp(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();

            return stream == null ? null : new StreamReader(stream).ReadToEnd();
        }
	}

    public struct Coords
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public struct JebSunTimes
    {
        public JebDateTime Sunrise { get; set; }
        public JebDateTime Sunset { get; set; }
    }

    public struct SunTimes
    {
        public DateTime Sunrise { get; set; }
        public DateTime Sunset { get; set; }
    }

	public class JebDateTime
	{
		private readonly Int64 _ticks;

        #region Constants

        // Number of 100ns ticks per time unit
		// 10000 / (1000 / 864)
		public const long TicksPerMillisecond = 8640;
        public const long TicksPerSecond = TicksPerMillisecond * 1000;
        public const long TicksPerMinute = TicksPerSecond * 100;
        public const long TicksPerHour = TicksPerMinute * 100;
        public const long TicksPerDay = TicksPerHour * 10;
        public const long TicksPerYear = TicksPerDay * 100;

		#endregion

		#region Constructors

        public JebDateTime(DateTime dateTime)
        {
            _ticks = dateTime.Subtract(new DateTime(1970, 1, 1)).Ticks;
        }

        public JebDateTime(long ticks)
        {
            _ticks = ticks;
        }

		public JebDateTime(int years, int days)
			: this(years, days, 0, 0, 0, 0)
		{
		}

		public JebDateTime(int years, int days, int hours, int minutes, int seconds)
			: this(years, days, hours, minutes, seconds, 0)
		{
		}

		public JebDateTime(int years, int days, int hours, int minutes, int seconds, int milliseconds)
		{
            _ticks =
                (TicksPerMillisecond * milliseconds)
                + (TicksPerSecond * seconds)
                + (TicksPerMinute * minutes)
                + (TicksPerHour * hours)
                + (TicksPerDay * days)
                + (TicksPerYear * years);
		}

		#endregion

        #region Static

        public static JebDateTime Now
		{
			get { return new JebDateTime(DateTime.Now); }
		}

        #endregion

        #region Properties

        #region Totals

        public long TotalTicks
	    {
	        get { return _ticks; }
	    }

        private double? _totalMilliseconds;
        public double TotalMilliseconds
        {
            get { return _totalMilliseconds ?? (_totalMilliseconds = _ticks / TicksPerMillisecond).Value; }
        }

        private double? _totalSeconds;
        public double TotalSeconds
        {
            get { return _totalSeconds ?? (_totalSeconds = _ticks / TicksPerSecond).Value; }
        }

        private double? _totalMinutes;
        public double TotalMinutes
        {
            get { return _totalMinutes ?? (_totalMinutes = _ticks / TicksPerMinute).Value; }
        }

        private double? _totalHours;
        public double TotalHours
        {
            get { return _totalHours ?? (_totalHours = _ticks / TicksPerHour).Value; }
        }

        private double? _totalDays;
        public double TotalDays
        {
            get { return _totalDays ?? (_totalDays = _ticks / TicksPerDay).Value; }
        }

        private double? _totalYears;
        public double TotalYears
        {
            get { return _totalYears ?? (_totalYears = _ticks / TicksPerYear).Value; }
        }

        #endregion

        #region Part

        private long? _years;
        public long Years
        {
            get { return _years ?? (_years = (long) TotalYears).Value; }
        }

        private byte? _days;
        public byte Days
        {
            get { return _days ?? (_days = (byte)Math.Floor((double)((_ticks % TicksPerYear) / TicksPerDay))).Value; }
        }

        private byte? _hours;
        public byte Hours
        {
            get { return _hours ?? (_hours = (byte)Math.Floor((double)((_ticks % TicksPerDay) / TicksPerHour))).Value; }
        }

        private byte? _minutes;
        public byte Minutes
        {
            get { return _minutes ?? (_minutes = (byte)Math.Floor((double)((_ticks % TicksPerHour) / TicksPerMinute))).Value; }
        }

        private byte? _seconds;
        public byte Seconds
        {
            get { return _seconds ?? (_seconds = (byte)Math.Floor((double)((_ticks % TicksPerMinute) / TicksPerSecond))).Value; }
        }

        private byte? _milliseconds;
        public byte Milliseconds
        {
            get { return _milliseconds ?? (_milliseconds = (byte)Math.Floor((double)((_ticks % TicksPerSecond) / TicksPerMillisecond))).Value; }
        }

        #endregion

        #endregion

        #region Adding

        public JebDateTime AddTicks(long value)
		{
			return new JebDateTime(_ticks + value);
		}

        public JebDateTime AddMilliseconds(double value)
		{
			return AddTicks((long) (value * TicksPerMillisecond));
		}

        public JebDateTime AddSeconds(double value)
        {
            return AddTicks((long)(value * TicksPerSecond));
		}

        public JebDateTime AddMinutes(double value)
        {
            return AddTicks((long)(value * TicksPerMinute));
		}

        public JebDateTime AddHours(double value)
        {
            return AddTicks((long)(value * TicksPerHour));
		}

        public JebDateTime AddDays(double value)
        {
            return AddTicks((long)(value * TicksPerDay));
		}

        public JebDateTime AddYears(int value)
        {
            return AddTicks((long)(value * TicksPerYear));
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0},{1:d2} {2:d1},{3:d2},{4:d2}", Years, Days, Hours, Minutes, Seconds);
        }

        #endregion
    }
}
